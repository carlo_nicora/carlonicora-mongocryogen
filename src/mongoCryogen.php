<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\mongoCryogen
 * @author Carlo Nicora
 */
namespace CarloNicora\cryogen\mongoCryogen;

use CarloNicora\cryogen\cryogen;
use CarloNicora\cryogen\entity;
use CarloNicora\cryogen\entityList;
use CarloNicora\cryogen\metaField;
use CarloNicora\cryogen\queryEngine;
use CarloNicora\cryogen\metaTable;

/**
 * Main class for the mongo plugin for cryogen
 *
 * @package CarloNicora\cryogen\mongoCryogen
 */
class mongoCryogen extends cryogen{
    /**
     * Initialises cryogen for mongo
     *
     * @param mongoConnectionBuilder $connection
     */
    public function __construct($connection){
        $returnValue = false;

        $this->connectionController = new mongoConnectionController();

        if ($this->connectionController->initialize($connection)){
            $this->structureController = new mongoStructureController($this->connectionController, $this);
            $returnValue = true;
        }

        return($returnValue);
    }

    /**
     * Returns if a database exists
     *
     * @param string $databaseName
     * @return bool
     */
    public function databaseExists($databaseName){
        return(true);
    }

    /**
     * @param metaTable|null $meta
     * @param entity|null $entity
     * @param null $valueOfKeyField
     * @return mixed
     */
    public function generateQueryEngine(metaTable $meta=null, entity $entity=null, $valueOfKeyField=null){
        $returnValue = new mongoQueryEngine($meta, $entity, $valueOfKeyField);

        return($returnValue);
    }

    /**
     * Clears the resources
     */
    public function __destruct(){
        if (isset($this->connectionController) && $this->connectionController->isConnected()){
            $this->connectionController->disconnect();
        } else if (isset($this->connectionController)){
            unset($this->connectionController);
        }
    }

    /**
     * Commit the INSERT, UPDATE or DELETE transaction on the database
     *
     * @param bool $commit
     * @return bool
     */
    protected function completeActionTransaction($commit){
        return (false);
    }

    /**
     * Returns the number of records matching the query in the query engine
     *
     * @param queryEngine $engine
     * @return int
     */
    public function count(queryEngine $engine) {
        $readStatement = $engine->generateReadStatement();
        $readParameters = $engine->generateReadParameters();

        /** @var \MongoCursor $cursor */
        if (isset($readStatement)) {
            $cursor = $this->connectionController->connection->selectCollection($engine->meta->name)->find($readStatement);
        } else {
            $cursor = $this->connectionController->connection->selectCollection($engine->meta->name)->find();
        }

        if (isset($readParameters) && sizeof($readParameters) > 0){
            if (isset($readParameters['order'])){
                $cursor->sort($readParameters['order']);
            }
            if (isset($readParameters['limit']['start'])){
                $cursor->skip($readParameters['limit']['start']);
            }
            if (isset($readParameters['limit']['length'])){
                $cursor->limit($readParameters['limit']['length']);
            }
        }

        return($cursor->count());
    }

    /**
     * Deletes an entity in the database.
     *
     * @param entity|entityList|null $entity
     * @param queryEngine|null $engine
     * @return bool
     */
    public function delete($entity = null, queryEngine $engine = null) {
        $returnValue = false;

        $entityList = null;

        if (isset($entity)) {
            if (isset($entity) && gettype($entity) != "array" && $entity->isEntityList) {
                $entityList = $entity;
            } else {
                if (isset($entity)) {
                    if (gettype($entity) != "array") {
                        $entityList = [];
                        $entityList[] = $entity;
                    } else {
                        $entityList = $entity;
                    }
                }
            }
        }

        if (isset($entityList)) {
            foreach ($entityList as $entity) {
                if (!method_exists($entity, 'rebuildFromObject')){
                    $newEntity = new entity();
                    $newEntity->metaTable = $entity->metaTable;
                    $newEntity->rebuildFromObject($entity);
                    $entity = $newEntity;
                }

                if (isset($entity)) {
                    $engine = $this->generateQueryEngine(null, $entity);
                }

                $deleteStatement = $engine->generateDeleteStatement();
                /** @var \MongoCursor $collection */
                if (isset($deleteStatement)) {
                    $mongoReturnValue = $this->connectionController->connection->selectCollection($engine->meta->name)->remove($deleteStatement);
                    $returnValue = ($mongoReturnValue['ok'] == 1);
                }
            }
        } else {
            $deleteStatement = $engine->generateDeleteStatement();
            /** @var \MongoCursor $collection */
            if (isset($deleteStatement)) {
                $mongoReturnValue = $this->connectionController->connection->selectCollection($engine->meta->name)->remove($deleteStatement);
                $returnValue = ($mongoReturnValue['ok'] == 1);
            }
        }


        return($returnValue);
    }

    /**
     * Reads a list of records identified by the query engine.
     *
     * If the levels of relations to load is > 0, then cryogen will load records related to a single foreign key as
     * defined in the database objects
     *
     * @param queryEngine $engine
     * @param int $levelsOfRelationsToLoad
     * @param metaTable|null $metaTableCaller
     * @param metaField|null $metaFieldCaller
     * @param bool $isSingle
     * @return entity|entityList|null
     */
    public function read(queryEngine $engine, $levelsOfRelationsToLoad = 0, metaTable $metaTableCaller = null, metaField $metaFieldCaller = null, $isSingle = false){
        $readStatement = $engine->generateReadStatement();
        $readParameters = $engine->generateReadParameters();

        /** @var \MongoCursor $cursor */
        if (isset($readStatement)) {
            $cursor = $this->connectionController->connection->selectCollection($engine->meta->name)->find($readStatement);
        } else {
            $cursor = $this->connectionController->connection->selectCollection($engine->meta->name)->find();
        }

        $returnValue = new entityList($engine->meta);

        if (isset($readParameters) && sizeof($readParameters) > 0){
            if (isset($readParameters['order'])){
                $cursor->sort($readParameters['order']);
            }
            if (isset($readParameters['limit']['start'])){
                $cursor->skip($readParameters['limit']['start']);
            }
            if (isset($readParameters['limit']['length'])){
                $cursor->limit($readParameters['limit']['length']);
            }
        }

        foreach ($cursor as $entity){
            /** @var entity $newEntity */

            $objectValue = json_decode(json_encode($entity));
            $newEntity = new $engine->meta->object;
            $newEntity->rebuildFromObject($objectValue);
            $entity = $newEntity;
            $entity->setInitialValues();
            $entity->setRetrieved();

            $returnValue[] = $entity;
        }

        return($returnValue);
    }

    /**
     * Updates an entity in the database.
     *
     * If the entity is not existing in the database, cryogen performs an INSERT, otherwise an UPDATE
     *
     * @param entity|entityList $entity
     * @return bool
     */
    public function update($entity) {
        $returnValue = false;

        if (isset($entity)) {
            if (isset($entity) && gettype($entity) != "array" && $entity->isEntityList) {
                $entityList = $entity;
            } else {
                if (isset($entity)) {
                    if (gettype($entity) != "array") {
                        $entityList = [];
                        $entityList[] = $entity;
                    } else {
                        $entityList = $entity;
                    }
                }
            }
        }

        if (isset($entityList)) {
            foreach ($entityList as $entity) {
                if (!method_exists($entity, 'rebuildFromObject')) {
                    $newEntity = new entity();
                    $newEntity->metaTable = $entity->metaTable;
                    $newEntity->rebuildFromObject($entity);
                    $entity = $newEntity;
                }

                if ($entity->status() != entity::ENTITY_NOT_MODIFIED) {
                    $engine = $this->generateQueryEngine(NULL, $entity);

                    if ($entity->status() == entity::ENTITY_NOT_RETRIEVED) {

                        if ($engine->hasAutoIncrementKey()) {
                            $keyField = $engine->getAutoIncrementKeyName();
                            $entity->$keyField = (string)new \MongoId();
                        }

                        $mongoReturnValue = $this->connectionController->connection->selectCollection($engine->meta->name)->insert($this->prepareEntity($entity));
                        $returnValue = ($mongoReturnValue['ok'] == 1);


                    } else {
                        $updateStatement = $engine->generateUpdateStatement();

                        $mongoReturnValue = $this->connectionController->connection->selectCollection($engine->meta->name)->update($updateStatement, $this->prepareEntity($entity));
                        $returnValue = ($mongoReturnValue['ok'] == 1);
                    }
                }

                $entity->setInitialValues();
                $entity->setRetrieved();
            }
        }

        return($returnValue);
    }

    /**
     * Prepares the standard object to be saved in the NoSQL database
     *
     * @param $entity
     * @return mixed
     */
    private function prepareEntity($entity){
        $returnValue = json_decode(json_encode($entity));
        unset($returnValue->entityStatus);
        unset($returnValue->entityRetrieved);
        unset($returnValue->metaTable);
        unset($returnValue->isEntityList);

        return($returnValue);
    }

    /**
     * Reads one single record identified by the query engine.
     *
     * If the query returns more than one record, the system generates an error. This function is designed to return
     * a single-record query, not the first of many records.
     * If the levels of relations to load is > 0, then cryogen will load records related to a single foreign key as
     * defined in the database objects
     *
     * @param queryEngine $engine
     * @param int $levelsOfRelationsToLoad
     * @param metaTable|null $metaTableCaller
     * @param metaField|null $metaFieldCaller
     * @return entity|null
     */
    public function readSingle(queryEngine $engine, $levelsOfRelationsToLoad = 0, metaTable $metaTableCaller = null, metaField $metaFieldCaller = null) {
        $returnValue = null;

        $globalReturn = $this->read($engine, $levelsOfRelationsToLoad, $metaTableCaller, $metaFieldCaller);

        if (isset($globalReturn) && sizeof($globalReturn) == 1) {
            $returnValue = $globalReturn[0];
        }

        return($returnValue);
    }

    /**
     * Runs the transactional INSERT, UPDATE or DELETE query on the database
     *
     * @param string $sqlStatement
     * @param array $sqlParameters
     * @param bool $isDelete
     * @param bool $generatedId
     * @return entityList
     */
    protected function setActionTransaction($sqlStatement, $sqlParameters, $isDelete = false, &$generatedId = false) {
        return(null);
    }

    /**
     * Specialised transaction that counts the records matching a specific query engine on the database
     *
     * @param queryEngine $engine
     * @param string $sqlStatement
     * @param array $sqlParameters
     * @return int
     */
    protected function setCountTransaction(queryEngine $engine, $sqlStatement, $sqlParameters) {
        return(0);
    }

    /**
     * Runs the transactional SELECT query on the database
     *
     * @param queryEngine $engine
     * @param string $sqlStatement
     * @param array $sqlParameters
     * @return entityList
     */
    protected function setReadTransaction(queryEngine $engine, $sqlStatement, $sqlParameters){
        return(null);
    }
}
?>