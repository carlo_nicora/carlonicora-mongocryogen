<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\mongoCryogen
 * @author Carlo Nicora
 */

namespace CarloNicora\cryogen\mongoCryogen;

use CarloNicora\cryogen\connectionController;
use MongoClient;
use MongoDB;

/**
 * Implementation of the connection controller for mongo
 *
 * @package CarloNicora\cryogen\mongoCryogen
 */
class mongoConnectionController extends connectionController{
    /**
     * @var mongoConnectionBuilder $connectionValues
     */
    public $connectionValues;

    /** @var MongoClient $client */
    public $client;

    /**
     * Opens a connection to the database
     *
     * @return bool
     */
    public function connect(){
        $returnValue = true;

        if (!isset($this->connection)) {
            if (isset($this->connectionValues->user) && $this->connectionValues->user != ''){
                $this->client = new MongoClient('mongodb://'.$this->connectionValues->user.':'.$this->connectionValues->password.'@'.$this->connectionValues->host);
            } else {
                $this->client = new MongoClient('mongodb://'.$this->connectionValues->host);
            }

            $this->connection = $this->client->selectDB($this->connectionValues->databaseName);
        }

        return($returnValue);
    }

    /**
     * Closes a connection to the database
     *
     * @return bool
     */
    public function disconnect(){
        if($this->isConnected()){
            $this->client->close();
        }
    }

    /**
     * Returns the name of the database specified in the connection
     *
     * @return string
     */
    public function getDatabaseName(){
        return($this->connectionValues->databaseName);
    }

    /**
     * Create a new Database
     *
     * @param string $databaseName
     * @return bool
     */
    public function createDatabase($databaseName){
        $this->connection = $this->client->selectDB($databaseName);

        return(isset($this->connection));
    }

    /**
     * Identifies if there is an active connection to the database
     *
     * @return bool
     */
    public function isConnected(){
        return(isset($this->connection));
    }
}