<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\mongoCryogen
 * @author Carlo Nicora
 */

namespace CarloNicora\cryogen\mongoCryogen;

use CarloNicora\cryogen\discriminant;
use CarloNicora\cryogen\queryEngine;

/**
 * The mongoQueryEngine specialises the queryEngine to prepare the queries specifically for mongo
 */
class mongoQueryEngine extends queryEngine{

    /**
     * Returns the type of field to be passed as type of parameters to mongo for the sql query preparation
     *
     * @param mixed $field
     * @return string
     */
    protected function getDiscriminantTypeCast($field=null){
        return(null);
    }

    /**
     * Generates the SQL statement needed for a count sql query
     *
     * @return string
     */
    public function generateReadCountStatement() {
        return($this->generateReadStatement());
    }

    /**
     * Geneates the SQL statement needed for a read sql query
     *
     * @return null|array
     */
    public function generateReadStatement() {
        $returnValue = [];

        $discriminantCount = sizeof($this->discriminants);

        $arrays = [];
        $arrayCounter = 0;

        for ($discriminantKey=0; $discriminantKey<$discriminantCount; $discriminantKey++){
            /** @var discriminant $discriminant */
            $discriminant = $this->discriminants[$discriminantKey];
            if (isset($discriminant->metaField)) {

                if ($discriminant->separator == '('){
                    $arrayCounter++;
                }

                $value = null;
                switch (strtolower(trim($discriminant->clause))){
                    case '!=':
                    case '<>':
                        if (isset($discriminant->value)){
                            $additionalArray = [];
                            $additionalArray['$ne']=$discriminant->value;
                            $value = $additionalArray;
                        } else {
                            $additionalArray = [];
                            $additionalArray['$ne']='';
                            $value = $additionalArray;
                        }
                        break;
                    case 'like':
                        $additionalArray = [];
                        $additionalArray['$regex']=new \MongoRegex("/$discriminant->value/");
                        $value = $additionalArray;
                        break;
                    case 'not like':
                        $additionalArray = [];
                        $additionalArray['$not']=new \MongoRegex("/$discriminant->value/");
                        $value = $additionalArray;
                        break;
                    case '>':
                        $additionalArray = [];
                        $additionalArray['$gt']=$discriminant->value;
                        $value = $additionalArray;
                        break;
                    case '>=':
                        $additionalArray = [];
                        $additionalArray['$gte']=$discriminant->value;
                        $value = $additionalArray;
                        break;
                    case '<':
                        $additionalArray = [];
                        $additionalArray['$lt'] = $discriminant->value;
                        $value = $additionalArray;
                        break;
                    case '<=':
                        $additionalArray = [];
                        $additionalArray['$lte'] = $discriminant->value;
                        $value = $additionalArray;
                        break;
                    case 'near':
                        $additionalArray = [];
                        $additionalArray['$near'] = [];
                        $additionalArray['$near']['$geometry'] = ['type'=>'Point', 'coordinates'=>[floatval($discriminant->value[0]),floatval($discriminant->value[1])]];
                        $additionalArray['$near']['$maxDistance'] = $discriminant->value[2];
                        $value = $additionalArray;
                        break;
                    case 'in':
                    case 'not in':
                        if (is_array($discriminant->value)){
                            $valueArray = $discriminant->value;
                        } else {
                            $valueArray = [];
                            $initialValueArray = explode(',', $discriminant->value);
                            foreach (isset($initialValueArray) ? $initialValueArray : [] as $initialValue){
                                $valueArray[] = $initialValue;
                            }
                        }
                        $additionalArray = [];
                        if (strtolower(trim($discriminant->clause)) == 'in'){
                            $additionalArray['$in'] = $valueArray;
                        } else {
                            $additionalArray['$nin'] = $valueArray;
                        }
                        $value = $additionalArray;
                        break;
                    default:
                        if (isset($discriminant->value)){
                            $additionalArray = [];
                            $additionalArray['$eq']=$discriminant->value;
                            $value = $additionalArray;
                        } else {
                            $additionalArray = [];
                            $additionalArray['$eq']=null;
                            $value = $additionalArray;
                        }
                        break;
                }

                $fieldName = $discriminant->metaField->name;

                if (strpos($fieldName, '/')){
                    list($fieldName, $fieldSubname) = explode('/', $fieldName);
                    $newValue = [];
                    $newSubValue = [];
                    $newSubValue[$fieldSubname] = $value;
                    $newValue['$elemMatch'] = $newSubValue;

                    $value = $newValue;
                }

                if (isset($arrays[$arrayCounter][$fieldName])){
                    if (array_keys($value)[0] == array_keys($arrays[$arrayCounter][$fieldName])[0]){
                        $submergedArray = array_merge(array_values($arrays[$arrayCounter][$fieldName])[0], array_values($value)[0]);
                        $mergedArray = [];
                        $mergedArray[array_keys($value)[0]] = $submergedArray;
                    } else {
                        $mergedArray = array_merge($arrays[$arrayCounter][$fieldName], $value);
                    }
                } else {
                    $mergedArray = array_merge([], $value);
                }
                $arrays[$arrayCounter][$fieldName] = $mergedArray;

                if ($discriminant->separator == ')'){
                    if ($discriminantKey != 0 && $this->discriminants[$discriminantKey-1]->connector == ' OR ' || $this->discriminants[$discriminantKey-1]->connector == 'OR'){
                        $newArray = [];
                        foreach ($arrays[$arrayCounter] as $key=>$value){
                            $newSubArray = [];
                            $newSubArray[$key] = $value;
                            $newArray[] = $newSubArray;
                        }
                        $arrays[$arrayCounter-1]['$or'] = $newArray;
                    } else {
                        $arrays[$arrayCounter-1][] = $arrays[$arrayCounter];
                    }
                    unset($arrays[$arrayCounter]);
                    $arrayCounter--;
                }

                if ($discriminantKey+1 == $discriminantCount) {
                    if ($discriminantKey != 0 && $discriminant->separator != ')' && ($this->discriminants[$discriminantKey-1]->connector == ' OR ' || $this->discriminants[$discriminantKey-1]->connector == 'OR')) {
                        $newArray = [];
                        foreach ($arrays[0] as $key=>$value){
                            $newSubArray = [];
                            $newSubArray[$key] = $value;
                            $newArray[] = $newSubArray;
                        }
                        $returnValue['$or'] = $newArray;
                    } else {
                        $returnValue = $arrays[0];
                    }
                }
            }
        }

        if (sizeof($returnValue) == 0) $returnValue = null;

        return($returnValue);
    }

    /**
     * Generates an array containing the parameters needed in WHERE or HAVING clauses
     *
     * @return array
     */
    public function generateReadParameters() {
        $returnValue = [];

        if (isset($this->ordering) && sizeof($this->ordering) > 0){
            $orderFields = [];
            foreach ($this->ordering as $order){
                $field = $order[0];

                if ($order[1]){
                    $orderFields[$field->name] = -1;
                } else {
                    $orderFields[$field->name] = 1;
                }
            }
            $returnValue['order'] = $orderFields;
        }

        if ($this->limitStart || $this->limitLength){
            $limits = [];
            $limits['start'] = $this->limitStart;
            $limits['length'] = $this->limitLength;

            $returnValue['limit'] = $limits;
        }

        return($returnValue);
    }

    /**
     * Generates the SQL statement needed for an UPDATE sql query
     *
     * @return string
     */
    public function generateUpdateStatement() {
        $returnValue = [];

        if (isset($this->keyFields) && sizeof($this->keyFields) > 0){
            foreach ($this->keyFields as $discriminant){
                if ($discriminant->isChanged){
                    $returnValue[$discriminant->metaField->name] = $discriminant->originalValue;
                } else {
                    $returnValue[$discriminant->metaField->name] = $discriminant->value;
                }
            }
        }

        if (sizeof($returnValue) == 0) $returnValue = null;

        return($returnValue);
    }

    /**
     * Generates an array containing the parameters needed in an UPDATE sql query
     *
     * @return array
     */
    public function generateUpdateParameters() {
        return(null);
    }

    /**
     * Generates the SQL statement needed for an INSERT sql query
     *
     * @return string
     */
    public function generateInsertStatement() {
        return(null);
    }

    /**
     * Generates an array containing the parameters needed in an INSERT sql query
     *
     * @return array
     */
    public function generateInsertParameters() {
        return(null);
    }

    /**
     * Generates the SQL statement needed for a DELETE sql query
     *
     * @return string
     */
    public function generateDeleteStatement() {
        $returnValue = [];

        if (isset($this->keyFields) && sizeof($this->keyFields) > 0){
            foreach($this->keyFields as $discriminant){
                if (isset($discriminant->value)){
                    $returnValue[$discriminant->metaField->name] = $discriminant->value;
                }
            }
        }

        if (sizeof($returnValue)==0) {
            if (isset($this->discriminants) && sizeof($this->discriminants) > 0){
                foreach($this->discriminants as $discriminant){
                    if (isset($discriminant->value)){
                        $returnValue[$discriminant->metaField->name] = $discriminant->value;
                    }
                }
            } else {
                foreach ($this->normalFields as $discriminant){
                    if (isset($discriminant->value)){
                        $returnValue[$discriminant->metaField->name] = $discriminant->value;
                    }
                }
            }
        }

        return($returnValue);
    }

    /**
     * Generates an array containing the parameters needed in aDELETE sql query
     *
     * @return array
     */
    public function generateDeleteParameters() {
        return(null);
    }
}